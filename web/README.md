# Web

## Overview

- [Web](https://gitlab.com/budgethub/web) is the web interface for [BudgetHub](https://www.budgethub.xyz).
- This chart provides deployment instructions to deploy Web to our cluster.
- This document contains instructions for launching Web to a Kubernetes cluster.

## Installing Helm

Assuming you're on a new Kubernetes cluster, we need to first setup [Helm](https://helm.sh), specifically, Tiller, to our cluster,
before we can start deploying the app to Kubernetes.

### Assigning your account as a cluster admin

If you're on [GKE](https://cloud.google.com/kubernetes-engine/), you need to set your account as a cluster admin so you can perform
admin-related roles on Kubernetes such as creating roles and role bindings.

```shell
kubectl create clusterrolebinding myname-cluster-admin-binding --clusterrole=cluster-admin --user=user@budgethub.xyz
```

The command above sets the user `user@budgethub.xyz` to be a `cluster-admin`.

### Creating the namespace and service account

We will configure Helm to deploy only [Web](https://gitlab.com/budgethub/web) for security purposes.

To start, we need to create the namespace to our cluster:

```shell
kubectl create namespace web
```

Next, create the service account that Tiller will use:

```shell
kubectl create serviceaccount tiller --namespace web
```

### Creating the role and rolebinding

In order to authorize Tiller to perform deployments on the `web` namespace, we need to define the role first:

```shell
kubectl create -f web/role-tiller.yaml
```

Next, we will assign the `tiller` service account to the `tiller-manager` role for the `web` namespace:

```shell
kubectl create -f web/rolebinding-tiller.yaml
```

### Generate certificates

We need to generate certificates so we can use SSL between Helm and Tiller.

Create a certificate authority by running these commands:

```shell
openssl genrsa -out web/certs/ca.key.pem 4096
openssl req -key web/certs/ca.key.pem -new -x509 -days 7300 -sha256 -out web/certs/ca.cert.pem -extensions v3_ca
```

Next, let's create the keys needed to generate the certificates:

```shell
# Tiller key
openssl genrsa -out web/certs/tiller.key.pem 4096

# Helm key. These can be created multiple times so you can have one per user
openssl genrsa -out web/certs/helm.key.pem 4096
```

We can now create the certificates from these keys:

```shell
openssl req -key web/certs/tiller.key.pem -new -sha256 -out web/certs/tiller.csr.pem
openssl req -key web/certs/helm.key.pem -new -sha256 -out web/certs/helm.csr.pem
```

Next, let's sign the CSRs with the CA certificate we created:

```shell
openssl x509 -req -CA web/certs/ca.cert.pem -CAkey web/certs/ca.key.pem -CAcreateserial -in web/certs/tiller.csr.pem -out web/certs/tiller.cert.pem -days 365
openssl x509 -req -CA web/certs/ca.cert.pem -CAkey web/certs/ca.key.pem -CAcreateserial -in web/certs/helm.csr.pem -out web/certs/helm.cert.pem  -days 365
```

### Install Tiller

Finally, we can install Tiller:

```shell
helm init --tiller-tls --tiller-tls-cert web/certs/tiller.cert.pem --tiller-tls-key web/certs/tiller.key.pem --tiller-tls-verify --tls-ca-cert web/certs/ca.cert.pem --service-account tiller --tiller-namespace web
```

### Create Docker Private Registry Secret

In order for Kubernetes to pull from Gitlab Registry, we need to create a new secret with the necessary credentials.

Generate a new deploy token from the repository's settings inside Gitlab.
It can be found in `https://gitlab.com/your-username/web/settings/repository`.

Assuming you were able to generate one, let's put it inside Kubernetes like so:

```shell
kubectl create secret docker-registry gitlab-registry --docker-server=registry.gitlab.com --docker-username=[DEPLOY_TOKEN] --docker-password=[DEPLOY_PASSWORD] --docker-email=[PRIMARY_GITLAB_EMAIL] -n web
```

## Deploying the App

We finally have everything we need to deploy the app!

Make sure that you've changed the values inside `web/values.yaml` and do the following:

```shell
helm install web --tiller-namespace web --namespace web --tls --tls-ca-cert web/certs/ca.cert.pem --tls-cert web/certs/helm.cert.pem --tls-key web/certs/helm.key.pem
```
