{{- define "web.release_labels" }}
{{- if .Values.staging }}
app: {{ printf "%s-staging-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- else }}
app: {{ printf "%s-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- end }}
version: {{ .Chart.Version }}
release: {{ .Release.Name }}
{{- end }}

{{- define "web.port_name" }}
{{- printf "web-web" }}
{{- end }}

{{- define "web.full_name" }}
{{- if .Values.staging }}
{{- printf "%s-staging-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- end }}
{{- end }}
