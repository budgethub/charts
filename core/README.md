# Core

## Overview

- [Core](https://gitlab.com/budgethub/core) provides the backend API for [BudgetHub](https://www.budgethub.xyz).
- This chart provides deployment instructions to deploy Core to our cluster.
- This document contains instructions for launching Core to a Kubernetes cluster.

## Installing Helm

Assuming you're on a new Kubernetes cluster, we need to first setup [Helm](https://helm.sh), specifically, Tiller, to our cluster,
before we can start deploying the app to Kubernetes.

### Assigning your account as a cluster admin

If you're on [GKE](https://cloud.google.com/kubernetes-engine/), you need to set your account as a cluster admin so you can perform
admin-related roles on Kubernetes such as creating roles and role bindings.

```shell
kubectl create clusterrolebinding myname-cluster-admin-binding --clusterrole=cluster-admin --user=user@budgethub.xyz
```

The command above sets the user `user@budgethub.xyz` to be a `cluster-admin`.

### Creating the namespace and service account

We will configure Helm to deploy only [Core](https://gitlab.com/budgethub/core) for security purposes.

To start, we need to create the namespace to our cluster:

```shell
kubectl create namespace core
```

Next, create the service account that Tiller will use:

```shell
kubectl create serviceaccount tiller --namespace core
```

### Creating the role and rolebinding

In order to authorize Tiller to perform deployments on the `core` namespace, we need to define the role first:

```shell
kubectl create -f core/role-tiller.yaml
```

Next, we will assign the `tiller` service account to the `tiller-manager` role for the `core` namespace:

```shell
kubectl create -f core/rolebinding-tiller.yaml
```

### Generate certificates

We need to generate certificates so we can use SSL between Helm and Tiller.

Create a certificate authority by running these commands:

```shell
openssl genrsa -out core/certs/ca.key.pem 4096
openssl req -key core/certs/ca.key.pem -new -x509 -days 7300 -sha256 -out core/certs/ca.cert.pem -extensions v3_ca
```

Next, let's create the keys needed to generate the certificates:

```shell
# Tiller key
openssl genrsa -out core/certs/tiller.key.pem 4096

# Helm key. These can be created multiple times so you can have one per user
openssl genrsa -out core/certs/helm.key.pem 4096
```

We can now create the certificates from these keys:

```shell
openssl req -key core/certs/tiller.key.pem -new -sha256 -out core/certs/tiller.csr.pem
openssl req -key core/certs/helm.key.pem -new -sha256 -out core/certs/helm.csr.pem
```

Next, let's sign the CSRs with the CA certificate we created:

```shell
openssl x509 -req -CA core/certs/ca.cert.pem -CAkey core/certs/ca.key.pem -CAcreateserial -in core/certs/tiller.csr.pem -out core/certs/tiller.cert.pem -days 365
openssl x509 -req -CA core/certs/ca.cert.pem -CAkey core/certs/ca.key.pem -CAcreateserial -in core/certs/helm.csr.pem -out core/certs/helm.cert.pem  -days 365
```

### Install Tiller

Finally, we can install Tiller:

```shell
helm init --tiller-tls --tiller-tls-cert core/certs/tiller.cert.pem --tiller-tls-key core/certs/tiller.key.pem --tiller-tls-verify --tls-ca-cert core/certs/ca.cert.pem --service-account tiller --tiller-namespace core
```

## Connecting to a Cloud SQL PostgreSQL database

Our deployment configurations require the use of a [Cloud SQL PostgreSQL database](https://cloud.google.com/sql/).
We need to provision one first and add the database and instance credentials as a secret.

### Enable the appropriate API

First step is to enable the Cloud SQL Admin API. None of the other steps will work if you don't enable this.

Go here: https://console.cloud.google.com/flows/enableapi?apiid=sqladmin&redirect=https://console.cloud.google.com&_ga=2.113982919.-139596153.1539409266

### Create a PostgreSQL Cloud SQL instance

Create the PostgreSQL instance so we can create databases and users for our app to use:

```shell
gcloud sql instances create --gce-zone asia-southeast1-1a --database-version POSTGRES_9_6 --memory 4 --cpu 2 budgethub
```

Afterwards, create the database for us to use:

```shell
gcloud sql databases create budget_hub_production --instance=budgethub
```

Finally, we need to set the password for the default `postgres` user:

```shell
gcloud sql users set-password postgres no-host --instance budgethub --password [INSERT_PASSWORD]
```

### Create a proxy user

We're going to connect to our PostgreSQL instance through a proxy so we need to create a service account for that:

```shell
gcloud iam service-accounts create proxy-user --display-name "proxy-user"
```

Get the email of the newly-generated service account:

```shell
gcloud iam service-accounts list
```

Once you've gotten the email, grant the service account that we generated earlier the `CloudSQL Client` role.
This will allow the service account to connect to the proxy on our behalf:

```shell
gcloud projets add-iam-policy-binding [PROJECT_ID] --member serviceAccount:[SERVICE_ACCOUNT_EMAIL] --role roles/cloudsql.client
```

Finally, we will download the credentials of the service account into `key.json` so we can use it for our Kubernetes deployment:

```shell
gcloud iam service-accounts keys create key.json --iam-account [SERVICE_ACCOUNT_EMAIL]
```

### Create Kubernetes secrets

This app's Kubernetes deployment requires the use of secrets to run the app.
Let's turn the credentials file that we downloaded in the previous step as a secret for our app to use:

```shell
kubectl create secret generic cloudsql-instance-credentials --from-file=credentials.json=./key.json
```

Next, we create the secret containing our database's credentials:

```shell
kubectl create secret generic db-credentials --from-literal=username=postgres --from-literal=password=[INSERT_PASSWORD]
```

## Deploying the App

We finally have everything we need to deploy the app!

Make sure that you've changed the values inside `core/values.yaml` and do the following:

```shell
helm install core --tiller-namespace core --namespace core --tls --tls-ca-cert core/certs/ca.cert.pem --tls-cert core/certs/helm.cert.pem --tls-key core/certs/helm.key.pem
```
