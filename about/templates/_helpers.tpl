{{- define "about.release_labels" }}
{{- if .Values.staging }}
app: {{ printf "%s-staging-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- else }}
app: {{ printf "%s-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- end }}
version: {{ .Chart.Version }}
release: {{ .Release.Name }}
{{- end }}

{{- define "about.port_name" }}
{{- printf "about-web" }}
{{- end }}

{{- define "about.full_name" }}
{{- if .Values.staging }}
{{- printf "%s-staging-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Chart.Name | trunc 63 }}
{{- end }}
{{- end }}
